<?php
    global $body_class;
    $body_class = 'front-page';
?>

<?php include 'partials/header.php'; ?>

<div id="scss-talk-welcome">

    <div>
        <h1>Let's Get SCSSy</h1>
    </div>

    <?php include 'partials/main-nav.php'; ?>

</div>


<?php include 'partials/footer.php'; ?>
<?php
    global $body_class;
    $body_class = 'front-page themes';
?>

<?php include 'partials/header.php'; ?>
    <div class="regular-theme theme">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, nostrum, aperiam ad in veritatis vel officiis ratione. A, quis, sed, sequi, placeat rerum modi esse sint natus tempore itaque facilis?</p>
    </div>

    <div class="medium-theme theme">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, quis, pariatur nostrum voluptates iusto reprehenderit. Id, quibusdam, nostrum, fuga vitae veritatis qui impedit illo iusto quam corrupti iste debitis placeat!</p>
    </div>

    <div class="dark-theme theme">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit quidem dolores repellendus sit qui obcaecati quasi nemo? Veritatis, consectetur, inventore, dolor, tenetur eaque commodi perferendis saepe blanditiis ea quidem pariatur!</p>
    </div>

    <div class="light-theme theme">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde facere eaque nulla perferendis aperiam quidem praesentium. Voluptate, repudiandae minus maxime molestias eos unde commodi tempora nostrum qui placeat odio similique.</p>
    </div>

<?php include 'partials/footer.php'; ?>
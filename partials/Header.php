<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"><!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex" />


    <title>Let's get SCSSy!</title>

    <?php // mobile meta ?>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" type="text/css" href="library/scss/style.css" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- START SCRIPTS -->
    <script src="http://code.jquery.com/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="library/js/home.js" type="text/javascript"></script>
    <script src="library/js/hamburger.js" type="text/javascript"></script>
    <!-- END SCRIPTS -->

</head>

<body class="<?php echo $body_class; ?>">

<div id="container" class="master-wrap">

    <div id="hamburger-menu">
        <h3>Menu</h3>
        <a class="return-home" href="/index.php">Home</a>
        <?php include 'partials/main-nav.php'; ?>
    </div>

    <header class="header" role="banner">
        <div class="scss-talk-authors">
            <span>Authors</span> <br />
            <a href="http://dillonmccallum.com/" title="Dillon">Dillon</a> <br />
            <a href="http://karine.do/" title="Karine Do">Karine</a>
        </div>
        <div class="logo-wrap">
            <a href="http://10up.com/"><img class="logo" src="library/images/svg/10up-Logo.svg"></a>
        </div>
        <a class="menu-icon" href="#">
            <span class="menu-icon__text">Show Menu</span>
        </a>
    </header>

    <div class="content-wrap fill-height">


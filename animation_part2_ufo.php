<?php
    global $body_class;
    $body_class = 'front-page';
?>

<?php include 'partials/header.php'; ?>

    <div class="animation-wrap ufo-bg">

        <div class="ufo">
            <img src="library/images/ufo.png">
        </div>

        <div class="lazers">
            <div class="lazer-1">
                <img src="library/images/lazer.png">
            </div>
            <div class="lazer-2">
                <img src="library/images/lazer.png">
            </div>
            <div class="lazer-3">
                <img src="library/images/lazer.png">
            </div>
        </div>

    </div>

<?php include 'partials/footer.php'; ?>
<?php
    global $body_class;
    $body_class = 'front-page';
?>

<?php include 'partials/header.php'; ?>

    <div class="animation-wrap super-j-bg">

        <img class="super-j-text" src="library/images/super-j-text.png">

        <div class="super-j">
            <img src="library/images/super-j.png">
        </div>

        <div class="clouds">
            <div class="cloud-1">
                <img src="library/images/cloud.png">
            </div>
            <div class="cloud-2">
                <img src="library/images/cloud2.png">
            </div>
            <div class="cloud-3">
                <img src="library/images/cloud2.png">
            </div>
        </div>

    </div>

<?php include 'partials/footer.php'; ?>
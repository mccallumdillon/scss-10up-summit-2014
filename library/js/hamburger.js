/**
 * hamburger.js
 *
 * Mobile Menu Hamburger
 * =====================
 * A hamburger menu for mobile websites
 *
 * Created by Chris Marslender & Dillon McCallum - http://10up.com/
 * Date: 16.07.13
 */

jQuery(document).ready(function() {

    //Open the menu
    jQuery(".menu-icon").click(function() {
        var master = jQuery('#container');

        master.bind('touchmove', function(e){e.preventDefault()});

        if ( master.hasClass('open-hamburger') ) {
            master.removeClass("open-hamburger");
        } else {
            master.addClass("open-hamburger");
        }

    });

    //Close the menu
    jQuery(".content-wrap").click(function() {
        var master = jQuery('#container');

        master.unbind('touchmove');
        master.removeClass("open-hamburger")
    });

    jQuery('.close-menu').click(function() {
        var master = jQuery('#master');

        master.unbind('touchmove');
        master.removeClass("open-hamburger");
    });

});
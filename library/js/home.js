$(document).ready(function() {

    // global vars
    var winHeight = $(window).height();
    console.log(winHeight);

    // set initial div height / width
    $('.fill-height').css({
        'min-height': winHeight,
    });

    // make sure div stays full width/height on resize
    $(window).resize(function() {
        $('.fill-height').css({
            'height': winHeight,
        });
    });

});
<?php
    global $body_class;
    $body_class = 'front-page';
?>

<?php include 'partials/header.php'; ?>

    <div class="animation-wrap">
        <div id="ballWrapper">
            <div id="ball"></div>
        </div>
    </div>

<?php include 'partials/footer.php'; ?>